package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public TextView pant;
    public double operan1, operan2, res;
    int opcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pant = (TextView) findViewById(R.id.caja);
    }

    public void bt1 (View v){
        String cap = pant.getText().toString();
        cap= cap + "1";
        pant.setText(cap);
    }

    public void bt2 (View v){
        String cap = pant.getText().toString();
        cap= cap + "2";
        pant.setText(cap);
    }

    public void bt3 (View v){
        String cap = pant.getText().toString();
        cap= cap + "3";
        pant.setText(cap);
    }

    public void bt4 (View v){
        String cap = pant.getText().toString();
        cap= cap + "4";
        pant.setText(cap);
    }

    public void bt5 (View v){
        String cap = pant.getText().toString();
        cap= cap + "5";
        pant.setText(cap);
    }

    public void bt6 (View v){
        String cap = pant.getText().toString();
        cap= cap + "6";
        pant.setText(cap);
    }

    public void bt7 (View v){
        String cap = pant.getText().toString();
        cap= cap + "7";
        pant.setText(cap);
    }

    public void bt8 (View v){
        String cap = pant.getText().toString();
        cap= cap + "8";
        pant.setText(cap);
    }

    public void bt9 (View v){
        String cap = pant.getText().toString();
        cap= cap + "9";
        pant.setText(cap);
    }

    public void bt0 (View v){
        String cap = pant.getText().toString();
        cap= cap + "0";
        pant.setText(cap);
    }

    public void btp (View v){
        String cap = pant.getText().toString();
        cap= cap + ".";
        pant.setText(cap);
    }

    public void btsuma (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("");
        opcion=1;
    }

    public void btmenos (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("");
        opcion=2;
    }

    public void btpor (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("");
        opcion=3;
    }

    public void btdiv (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("");
        opcion = 4;
    }

    public void btsin (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("sin("+operan1+")");
        opcion=5;
    }

    public void btcos (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("cos("+operan1+")");
        opcion=6;
    }

    public void bttan (View v){
        try{
            String aux1 = pant.getText().toString();
            operan1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pant.setText("tan("+operan1+")");
        opcion=7;
    }

    @SuppressLint("SetTextI18n")
    public void bti (View v){
        try{
            String aux2 = pant.getText().toString();
            operan2 = Double.parseDouble(aux2);
        }catch (NumberFormatException nfe){}
        pant.setText("");

        if (opcion == 1){
            res = operan1+operan2;
        }else if (opcion == 2){
            res = operan1-operan2;
        }else if (opcion == 3){
            res = operan1*operan2;
        }else if (opcion == 4){
            if(operan2 != 0.0){
                res = operan1/operan2;
            }
        }else if (opcion == 5){
            double rad =  Math.toRadians(operan1);
            res = (Math.sin(rad));
        }else if (opcion == 6){
            double rad =  Math.toRadians(operan1);
            res = (Math.cos(rad));
        }else if (opcion == 7){
            double rad =  Math.toRadians(operan1);
            res = (Math.tan(rad));
        }

        if(opcion == 4 && operan2 == 0.0){
            pant.setText("Error Matematico");
        }else {
            pant.setText("" + res);
        }
        operan1 = res;
    }

    public void btdelete (View v){
        pant.setText("");
        operan1= 0.0;
        operan2= 0.0;
        res = 0.0;
    }

    public void btdel (View v){
        if (!pant.getText().toString().equals("")){
            pant.setText(pant.getText().subSequence(0, pant.getText().length()-1)+"");
        }
    }
}